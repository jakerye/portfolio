#! /bin/bash

# Get parameters
portfolio_path=$1

# Validate parameters
[ ! -z "$portfolio_path" ] || (echo "Path to the portfolio.wiki is required" && exit)

# Go to portfolio
cd $portfolio_path

# Initialize portfolio summary pages
appendix_pages=(OpenAg-Device-Software-Codebase OpenAg-Device-Software-Code-Structure OpenAg-Recipe-Design OpenAg-Light-Design)

# Create temporary portfolio path
[ -d /tmp/portfolio ] || mkdir -p /tmp/portfolio

# Create cover page pdf
# echo -e "\pagenumbering{gobble}\n\n$(cat home.md)" > f-home.md
# pandoc f-home.md -o ~/Desktop/home.pdf -V geometry:margin=0.7in -fmarkdown-implicit_figures

# Create individual pdfs
for page in ${appendix_pages[@]}; do

  # Format page
  title=${page//-/ }
  echo -e "\pagenumbering{gobble}\n\n#$title\n$(cat $page.md)" >f-$page.md

  # Convert page
  # --variable urlcolor=blue
done

pandoc -s f-OpenAg-Device-Software-Codebase.md f-OpenAg-Device-Software-Code-Structure.md f-OpenAg-Recipe-Design.md f-OpenAg-Light-Design.md -o ~/Desktop/appendix.pdf -V geometry:margin=0.7in -fmarkdown-implicit_figures

# TODO: Concatenate pdfs into a single doc

# TODO: Clean up
