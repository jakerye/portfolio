## Jake Rye's Portfolio
![the_general_problem](assets/the_general_problem.jpg)
*"I find that when someone's taking time to do something right in the present, they're a perfectionist with no ability to prioritize, whereas when someone took time to do something right in the past, they're a master artisan of great foresight." ~ xkcd.com/974/*

## About
I'm a software & embedded systems engineer currently living in Boston, MA. Product development is one of my favorite things to do. Whether I'm building a new app, designing electromechanical systems, or exploring no-code tools, I'm usually having a good time doing it. Check out some of my previous projects to get a better idea of how I think and like to solve problems.

## Projects
- **[OpenAg Device Software](https://gitlab.com/jakerye/portfolio/wikis/OpenAg-Device-Software)** - Software for controlling plant growth chambers. (Python, Django, DRF).
- **[OpenAg Education UI](https://gitlab.com/jakerye/portfolio/wikis/OpenAg-Education-UI)** - Interface for educators to operate plant growth chambers. (Javascript, React)
- **[OpenAg Hardware Ecosystem](https://gitlab.com/jakerye/portfolio/wikis/OpenAg-Hardware-Ecosystem)** - Hardware for sensing and actuating plant growth chambers. (Eagle)
- **[OpenAg Data Infrastructure 2.0](https://gitlab.com/jakerye/portfolio/wikis/OpenAg-Data-Infrastructure-2.0)** - Proposal for the next generation data infrastructure. (GCP, Firebase)
- **[Food Button](https://gitlab.com/jakerye/portfolio/wikis/Food-Button)** - Mobile app for ordering takeout from Alexa & IFTTT. (Javascript, React Native, GCP, Firebase)
- **[Craft Caffeine](https://gitlab.com/jakerye/portfolio/wikis/Craft-Caffeine)** - E-commerce affiliate store for caffeinated products. (Liquid, Shopify)
- **[TandyGram](https://gitlab.com/jakerye/portfolio/wikis/TandyGram)** - Web app for printing photos by texting a phone number. (Python, Django, Javascript, Heroku)
- **[UMD VR Treadmill](https://gitlab.com/jakerye/portfolio/wikis/UMD-VR-Treadmill)** - Slip-based treadmill prototype to walk in a virtual environment. (C#, Unity)
- **[UMD Hovercraft](https://gitlab.com/jakerye/portfolio/wikis/UMD-Hovercraft)** - Miniature hovercraft prototype using optical mice for localization. (C#, ROS)

## Usage
This portfolio is a hybrid digital-physical document that exists as both an [online wiki](https://gitlab.com/jakerye/portfolio/wikis/home) and an auto-generated pdf document set containing a [portfolio summary](https://gitlab.com/jakerye/portfolio/blob/master/portfolio.pdf) and a [porfolio appendix](https://gitlab.com/jakerye/portfolio/blob/master/appendix.pdf). In general, the online wiki is a bit easier to navigate but having a physical document that can be printed out can be convenient. The portfolio summary contains project one-pagers highlighting interesting content about each project. The portfolio appendix contains in-depth information (e.g. code samples, design documents, architecture diagrams, etc.) referenced in the project one-pagers.

