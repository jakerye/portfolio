import os, shutil, subprocess

from scrape_project_page import scrape_project_page
from generate_project_appendix_cover import generate_project_appendix_cover
from generate_pdf_from_markdown import (
    generate_pdf_from_markdown_file,
    generate_pdf_from_markdown_files,
)
from concatenate_pdfs import concatenate_pdfs
from generate_appendix_cover import generate_appendix_cover

# Run main
if __name__ == "__main__":
    project_path = "../portfolio.wiki"
    project_names = [
        "OpenAg-Device-Software",
        "OpenAg-Education-UI",
        "OpenAg-Hardware-Ecosystem",
        "OpenAg-Data-Infrastructure-2.0",
        "Food-Button",
        "Craft-Caffeine",
        "TandyGram",
        "UMD-VR-Treadmill",
        "UMD-Hovercraft",
    ]

    # Generate portfolio cover pdf
    generate_pdf_from_markdown_file(project_path, "home")

    # Set portfolio project readme as portfolio cover
    print("Generating: README")
    with open("../portfolio.wiki/f-home.md", "r") as home_file:
        lines = home_file.readlines()
        lines[3] = '![the_general_problem](assets/the_general_problem.jpg)'
        with open("README.md", "w") as readme_file:
            readme_file.writelines(lines[2:])

    # Generate portfolio project pdfs
    for project_name in project_names:
        generate_pdf_from_markdown_file(project_path, project_name, add_title=True)

    # Concatenate portfolio pdfs
    concatenate_pdfs(project_path, "portfolio", ["home"] + project_names)

    # Initialize appendix lists
    appendix_cover_content = []
    appendix_project_names = []

    # Generate project appendix pdfs
    for project_name in project_names:

        # Scrape links from project page
        cover_image_link, external_links, internal_links = scrape_project_page(
            project_path, project_name,
        )

        # Check if project has requires an appendix
        if len(external_links) < 1 and len(internal_links) < 1:
            print(f"{project_name} does not require an appendix")
            continue
        appendix_project_names.append(project_name)

        # Generate project appendix cover pdf
        generate_project_appendix_cover(
            project_path,
            project_name,
            cover_image_link,
            external_links,
            internal_links,
        )
        generate_pdf_from_markdown_file(project_path, f"{project_name}-Appendix-Cover")

        # Generate project appendix subpage pdfs (Page break after each document)
        internal_link_paths = []
        for internal_link in internal_links:
            internal_link_path = internal_link.get("path")
            if internal_link_path:
                internal_link_paths.append(internal_link_path)
                generate_pdf_from_markdown_file(
                    project_path, internal_link_path, add_title=True,
                )
        concatenate_pdfs(
            project_path, f"{project_name}-Appendix-Subpages", internal_link_paths,
        )

        # Concatenate project appendix pdfs
        concatenate_pdfs(
            project_path,
            f"{project_name}-Appendix",
            [f"{project_name}-Appendix-Cover", f"{project_name}-Appendix-Subpages"],
        )

        # Update appendix cover content
        print(f"Updating appendix cover content to include: {project_name}")

        appendix_cover_content.append(
            {"project_name": project_name, "internal_link_paths": internal_link_paths,}
        )

    # Generate appendix cover
    generate_appendix_cover(project_path, appendix_cover_content)
    generate_pdf_from_markdown_file(project_path, "Appendix-Cover")

    # Concatenate appendix pdfs
    appendix_names = []
    for project_name in appendix_project_names:
        appendix_names.append(f"{project_name}-Appendix")
    concatenate_pdfs(project_path, "appendix", ["Appendix-Cover"] + appendix_names)

    # Store copy of portfolio and appendix in this repo
    shutil.copyfile(f"{project_path}/portfolio.pdf", "./portfolio.pdf")
    shutil.copyfile(f"{project_path}/appendix.pdf", "./appendix.pdf")
