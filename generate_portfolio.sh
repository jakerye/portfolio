#! /bin/bash

# Get parameters
portfolio_path=$1

# Validate parameters
[ ! -z "$portfolio_path" ] || (echo "Path to the portfolio.wiki is required" && exit)

# Go to portfolio
cd $portfolio_path

# Initialize portfolio summary pages
summary_pages=(OpenAg-Device-Software OpenAg-Hardware-Ecosystem)

# Create temporary portfolio path
[ -d /tmp/portfolio ] || mkdir -p /tmp/portfolio

# Create cover page pdf
echo -e "\pagenumbering{gobble}\n\n$(cat home.md)" > f-home.md
pandoc f-home.md -o ~/Desktop/home.pdf -V geometry:margin=0.7in -fmarkdown-implicit_figures

# Create individual pdfs
for page in ${summary_pages[@]}; do

  # Format page
  title=${page//-/ }
  echo -e "\pagenumbering{gobble}\n\n#$title\n$(cat $page.md)" > f-$page.md
  
  # Convert page
  pandoc f-$page.md -o ~/Desktop/$page.pdf -V geometry:margin=0.7in -fmarkdown-implicit_figures
  # --variable urlcolor=blue
done

# TODO: Concatenate pdfs into a single doc


# TODO: Clean up
