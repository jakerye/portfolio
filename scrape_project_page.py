import re


def scrape_project_page(project_path, project_name):
    print(f"Scraping: {project_name}")
    project_file = open(f"{project_path}/{project_name}.md", "r")
    markdown = project_file.read()

    # Get all links
    links = re.findall(r"\[(.*?)\]\((.*?)\)", markdown)

    # Parse links
    cover_image_link = None
    internal_links = []
    external_links = []
    for link in links:
        link_name, link_path = link[0], link[1]

        # Get cover image link
        if not cover_image_link and link_path.endswith((".png", ".jpg", ".jpeg")):
            cover_image_link = {"name": link_name, "path": link_path}
            continue

        # Get internal links
        if link_path.startswith("http"):
            external_links.append({"name": link_name, "path": link_path})

        # Get external links
        else:
            internal_links.append({"name": link_name, "path": link_path})
    
    # Clean up
    project_file.close()

    # Return results
    return cover_image_link, external_links, internal_links


# if __name__ == "__main__":
#     project_path = "../portfolio.wiki"
#     project_name = "OpenAg-Device-Software"

#     cover_image_link, external_links, internal_links = scrape_project_page(
#         project_path, project_name,
#     )
#     print(f"cover_image_link: {cover_image_link}")
#     print(f"external_links: {external_links}")
#     print(f"internal_links: {internal_links}")

