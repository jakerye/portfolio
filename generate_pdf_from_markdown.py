import subprocess, re


def format_markdown_file_backup(file_path, file_name, add_title=False):
    # Build formatting header
    header = "\pagenumbering{gobble}\n\n"
    if add_title:
        header += f"# {file_name.replace('-', ' ')}\n"

    # Prepend formatting header
    with open(f"{file_path}/{file_name}.md", "r") as old_file:
        content = old_file.read()
        with open(f"{file_path}/f-{file_name}.md", "w") as new_file:
            new_file.write(header + content)


def format_markdown_file(file_path, file_name, add_title=False):
    # Build formatting header
    header = "\pagenumbering{gobble}\n\n"
    if add_title:
        header += f"# {file_name.replace('-', ' ')}\n"

    # Format line by line
    with open(f"{file_path}/{file_name}.md", "r") as old_file:
        lines = old_file.readlines()
        with open(f"{file_path}/f-{file_name}.md", "w") as new_file:
            # Prepend formatting header
            new_file.write(header)

            # Replace local wiki links with global wiki links
            for line in lines:
                links = re.findall(r"\[(.*?)\]\((.*?)\)", line)
                if len(links) > 0:
                    for link in links:
                        if (
                            "upload" not in link[1]
                            and "http" not in link[1]
                            and not link[1].endswith(".jpg")
                            and not "#" in link[1]
                        ):
                            old_link = f"[{link[0]}]({link[1]})"
                            new_link = f"[{link[0]}](https://gitlab.com/jakerye/portfolio/wikis/{link[1]})"
                            line = line.replace(old_link, new_link)
                new_file.write(line)


def generate_pdf_from_markdown_file(file_path, file_name, add_title=False):
    print(f"Generating PDF: {file_name}")
    format_markdown_file(file_path, file_name, add_title=add_title)

    # Run command
    command = [
        "pandoc",
        f"f-{file_name}.md",
        "-o",
        f"{file_name}.pdf",
        "-V",
        "geometry:margin=0.7in",
        "-V",
        "urlcolor=blue",
        "-V",
        "linkcolor=black",
        "-fmarkdown-implicit_figures",
    ]
    subprocess.run(command, cwd=file_path)


def generate_pdf_from_markdown_files(
    file_path, file_names, output_file_name, add_title=False
):
    print(f"Generating PDF: {file_names}")
    command = ["pandoc", "-s"]
    for file_name in file_names:
        format_markdown_file(file_path, file_name, add_title=add_title)
        command.append(f"f-{file_name}.md")
    command += [
        "-o",
        f"{output_file_name}.pdf",
        "-V",
        "geometry:margin=0.7in",
        "-V",
        "urlcolor=blue",
        "-V",
        "linkcolor=black",
        "-fmarkdown-implicit_figures",
    ]
    subprocess.run(command, cwd=file_path)
