def generate_project_appendix_cover(
    project_path, project_name, cover_image_link, external_links, internal_links
):
    # Get parameters
    project_appendix_cover_name = f"{project_name}-Appendix-Cover"
    print(f"Generating: {project_appendix_cover_name}")


    # Open file
    project_cover_file = open(f"{project_path}/{project_appendix_cover_name}.md", "w")

    # Add formatting
    project_cover_file.write("\pagenumbering{gobble}\n\n")
    
    # Add title
    project_cover_file.write(f"# {project_name.replace('-', ' ')} Appendix\n")

    # Add cover image
    if project_cover_file:
        cover_image_name = cover_image_link.get("name")
        cover_image_path = cover_image_link.get("path")
        project_cover_file.write(f"\n![{cover_image_name}]({cover_image_path})\n")

    # Add contents
    if len(internal_links) > 0:
        project_cover_file.write("\n## Contents\n")
        for index, internal_link in enumerate(internal_links):
            internal_link_name = internal_link.get("name")
            project_cover_file.write(f"{index+1}. {internal_link_name.title()}\n")

    # Add external links
    if len(external_links) > 0:
        project_cover_file.write("\n## External Links\n")
        for index, external_link in enumerate(external_links):
            external_link_name = external_link.get("name")
            external_link_path = external_link.get("path")
            project_cover_file.write(
                f"{index+1}. {external_link_name.title()}: {external_link_path}\n"
            )

    # Clean up
    project_cover_file.close()


# if __name__ == "__main__":
#     project_path = "../portfolio.wiki"
#     project_name = "OpenAg-Device-Software"
#     cover_image_link = {
#         "name": "iot_architecture",
#         "path": "uploads/62061a43ccae2f97c74d35c0efe4f5d6/iot_architecture.png",
#     }
#     external_links = [
#         {
#             "name": "codebase",
#             "path": "https://github.com/OpenAgricultureFoundation/openag-device-software",
#         },
#         {
#             "name": "code structure diagram",
#             "path": "https://gitlab.com/jakerye/portfolio/blob/master/assets/openag-device-software/code_structure.png",
#         },
#         {
#             "name": "recipe example",
#             "path": "https://gitlab.com/jakerye/portfolio/snippets/1907471",
#         },
#     ]
#     internal_links = [
#         {"name": "recipe design", "path": "openag-recipe-design"},
#         {"name": "light design", "path": "openag-light-design"},
#     ]

#     generate_project_appendix_cover(
#         project_path, project_name, cover_image_link, external_links, internal_links,
#     )
