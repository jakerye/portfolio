from PyPDF2 import PdfFileMerger

def concatenate_pdfs(file_path, file_name, pdf_names):
    merger = PdfFileMerger()
    for pdf_name in pdf_names:
        merger.append(f"{file_path}/{pdf_name}.pdf")
    merger.write(f"{file_path}/{file_name}.pdf")
    merger.close()
