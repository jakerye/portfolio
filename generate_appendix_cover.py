def generate_appendix_cover(project_path, appendix_cover_content):
    print("Generating appendix cover")

    # Open file
    appendix_project_file = open(f"{project_path}/Appendix-Cover.md", "w")

    # Add formatting
    appendix_project_file.write("\pagenumbering{gobble}\n\n")

    # Add title
    appendix_project_file.write("# Jake Rye's Portfolio Appendix\n")

    # Add cover image
    appendix_project_file.write("\n![Apendix Joke](../portfolio/assets/appendix_joke.jpg)\n")
    appendix_project_file.write("\n*~ @Chumworth*\n")

    # Add contents
    appendix_project_file.write("\n## Contents\n")
    for index, project_entry in enumerate(appendix_cover_content):
        project_name = project_entry.get("project_name")
        internal_link_paths = project_entry.get("internal_link_paths")
        appendix_project_file.write(f"{index+1}. {project_name.replace('-', ' ')}\n")
        for index2, link_path in enumerate(internal_link_paths):
          appendix_project_file.write(f"\t{index2+1}. {link_path.replace('-', ' ')}\n")

    # Clean up
    appendix_project_file.close()
